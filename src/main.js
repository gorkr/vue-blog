import Vue from 'vue'
import App from './App.vue'
import VueResource from 'vue-resource'
import VueRouter from 'vue-router'
import Routes from './routers'

Vue.config.productionTip = false

Vue.use(VueResource);
Vue.use(VueRouter)

// // 自定义指令
// Vue.directive('rainbow', {
//   bind(el) {
//     // el 代表Dom元素
//     // bing 拿到指令的参数

//     el.stule.color = "#" + Math.random().toString(16).slice(2,8);
//   }

// });

Vue.directive('theme', {
  bind(el, binding) {
    if(binding.value == 'wide'){
      el.style.maxWidth = "1250px";
    }else if(binding.value == 'narrow'){
      el.style.maxWidth = "560px";
    }

    if(binding.arg == 'column'){
      el.style.background = "#6677cc";
      el.style.padding = "20px";
    }
  }
});


// // 自定义过滤器
// Vue.filter("to-uppercase", function(value){
//   return value.toUpperCase();
// })

Vue.filter("snippet", function(value){
  return value.slice(0,100) + "...";
})


// 创建路由
const router = new VueRouter({
  routes:Routes,
  mode: "history"

});

new Vue({
  render: h => h(App),
  router: router
}).$mount('#app')
